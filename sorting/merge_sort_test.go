package sorting

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

// MergeSort
func TestMergeSortEmpty(t *testing.T) {
    var array []int64

    array = getEmptyArray()
    MergeSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Empty not working")
}

func TestMergeSortOrdered(t *testing.T) {
    var array []int64

    array = getOrderedArray()
    MergeSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Ordered not working")
}

func TestMergeSortReverse(t *testing.T) {
    var array []int64

    array = getReverseOrderedArray()
    MergeSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Reverse ordered not working")
}

func TestMergeSortShuffled(t *testing.T) {
    var array []int64

    array = getShuffledArray()
    MergeSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Shuffled not working")
}

