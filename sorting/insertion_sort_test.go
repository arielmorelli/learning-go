package sorting

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

// InsertionSort
func TestInsertionSortEmpty(t *testing.T) {
    var array []int64

    array = getEmptyArray()
    InsertionSort(array)
    assert.True(t, isSorted(array), "Empty not working")
}

func TestInsertionSortOrdered(t *testing.T) {
    var array []int64

    array = getOrderedArray()
    InsertionSort(array)
    assert.True(t, isSorted(array), "Ordered not working")
}

func TestInsertionSortReverse(t *testing.T) {
    var array []int64

    array = getReverseOrderedArray()
    InsertionSort(array)
    assert.True(t, isSorted(array), "Reverse ordered not working")
}

func TestInsertionSortShuffled(t *testing.T) {
    var array []int64

    array = getShuffledArray()
    InsertionSort(array)
    assert.True(t, isSorted(array), "Shuffled not working")
}

