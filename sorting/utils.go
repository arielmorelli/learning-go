package sorting

func isSorted(array []int64) bool {
    for i := 1; i < len(array); i++ {
        if array[i] < array[i-1] {
            return false
        }
    }
    return true
}

