package sorting

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

// BubbleSort
func TestBubbleSortEmpty(t *testing.T) {
    var array []int64

    array = getEmptyArray()
    BubbleSort(array)
    assert.True(t, isSorted(array), "Empty not working")
}

func TestBubbleSortOrdered(t *testing.T) {
    var array []int64

    array = getOrderedArray()
    BubbleSort(array)
    assert.True(t, isSorted(array), "Ordered not working")
}

func TestBubbleSortReverse(t *testing.T) {
    var array []int64

    array = getReverseOrderedArray()
    BubbleSort(array)
    assert.True(t, isSorted(array), "Reverse ordered not working")
}

func TestBubbleSortShuffled(t *testing.T) {
    var array []int64

    array = getShuffledArray()
    BubbleSort(array)
    assert.True(t, isSorted(array), "Shuffled not working")
}

