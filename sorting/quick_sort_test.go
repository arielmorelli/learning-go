package sorting

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

// QuickSort
func TestQuickSortEmpty(t *testing.T) {
    var array []int64

    array = getEmptyArray()
    QuickSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Empty not working")
}

func TestQuickSortOrdered(t *testing.T) {
    var array []int64

    array = getOrderedArray()
    QuickSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Ordered not working")
}

func TestQuickSortReverse(t *testing.T) {
    var array []int64

    array = getReverseOrderedArray()
    QuickSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Reverse ordered not working")
}

func TestQuickSortShuffled(t *testing.T) {
    var array []int64

    array = getShuffledArray()
    QuickSort(array, 0, len(array)-1)
    assert.True(t, isSorted(array), "Shuffled not working")
}

