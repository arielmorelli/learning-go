package sorting

func merge(array []int64, init_pos, half_pos int, end_pos int) {
    n1 := half_pos - init_pos + 1
    n2 := end_pos - half_pos

    first_array := make([]int64, n1)
    second_array := make([]int64, n2)
    copy(first_array, array[init_pos:half_pos])
    copy(second_array, array[half_pos+1:end_pos])

    i := 0
    j := 0
    current_pos := init_pos
    for ; i < n1 && j < n2 ; current_pos++ {
        if first_array[i] < second_array[j] {
            array[current_pos] = first_array[i]
            i++
        } else {
            array[current_pos] = second_array[j]
            j++
        }
    }

    for ; i < n1 ; i++ {
        array[current_pos] = first_array[i]
        current_pos++
    }

    for ; j < n2 ; j++ {
        array[current_pos] = second_array[j]
        current_pos++
    }
}

func MergeSort(array []int64, init_pos int, end_pos int) {
    if init_pos >= end_pos {
        return
    }

    half_pos := (end_pos + init_pos)/2
    MergeSort(array, init_pos, half_pos)
    MergeSort(array, half_pos+1, end_pos)

    merge(array, init_pos, half_pos, end_pos)
}

