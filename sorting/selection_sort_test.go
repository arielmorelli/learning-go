package sorting

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

// SelectionSort
func TestSelectionSortEmpty(t *testing.T) {
    var array []int64

    array = getEmptyArray()
    SelectionSort(array)
    assert.True(t, isSorted(array), "Empty not working")
}

func TestSelectionSortOrdered(t *testing.T) {
    var array []int64

    array = getOrderedArray()
    SelectionSort(array)
    assert.True(t, isSorted(array), "Ordered not working")
}

func TestSelectionSortReverse(t *testing.T) {
    var array []int64

    array = getReverseOrderedArray()
    SelectionSort(array)
    assert.True(t, isSorted(array), "Reverse ordered not working")
}

func TestSelectionSortShuffled(t *testing.T) {
    var array []int64

    array = getShuffledArray()
    SelectionSort(array)
    assert.True(t, isSorted(array), "Shuffled not working")
}

