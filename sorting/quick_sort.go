package sorting

func partition(array []int64, init_pos int, end_pos int) int {
    var temp int64
    pivot := array[end_pos]

    i := init_pos
    for j := init_pos; j < end_pos; j++ {
        if array[j] < pivot {
            temp = array[i]
            array[i] = array[j]
            array[j] = temp
            i++
        }
    }

    temp = array[i]
    array[i] = array[end_pos]
    array[end_pos] = temp

    return i
}

func QuickSort(array []int64, init_pos int, end_pos int) {
    if init_pos >= end_pos {
        return
    }

    pivot_pos := partition(array, init_pos, end_pos)

    QuickSort(array, init_pos, pivot_pos-1)
    QuickSort(array, pivot_pos+1, end_pos)
}

