package sorting

func InsertionSort(array []int64) {
    var key int64
    var j int

    for i := 1; i < len(array); i++ {
        key = array[i]

        for j = i - 1; j > 0 && key < array[j]; j-- {
            array[j+1] = array[j]
        }
        array[j] = key
    }
}

