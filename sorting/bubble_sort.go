package sorting

import (
)

func BubbleSort(array []int64) {
    var tempVar int64

    for i := 0; i < len(array); i++ {
        for j := 0; j < len(array)-i-1; j++ {
            if array[j] > array[j+1] {
                tempVar = array[j]
                array[j] = array[j+1]
                array[j+1] = tempVar
            }
        }
    }
}
