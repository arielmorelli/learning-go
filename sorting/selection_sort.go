package sorting

func SelectionSort(array []int64) {
    var temp int64
    var min_index, i, j int

    arrayLen := len(array)

    for i = 0; i < arrayLen-1; i++ {
        min_index = i

        for j = i+1; j < arrayLen; j++ {
            if array[j] < array[min_index] {
                min_index = j
            }
        }

        temp = array[i]
        array[i] = array[min_index]
        array[min_index] = temp
    }
}

