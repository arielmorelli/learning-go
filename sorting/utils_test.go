package sorting

import (
    "testing"
    "github.com/stretchr/testify/assert"
)

// isSorted
func TestIsSortedTrue(t *testing.T) {
    assert.True(t, isSorted([]int64{1,2,3,4,5,6}))
}

func TestIsSortedFalse(t *testing.T) {
    assert.False(t, isSorted([]int64{10,7,2,5,3,1}))
}

