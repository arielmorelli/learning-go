package main

import (
    "fmt"
    "os"
    "strconv"
    "math/rand"
    "math"
    "time"
)


func main() {
    var i int64

    max := int64(math.Pow10(4))

    fmt.Println("create file")
    f, _ := os.Create("sorting/variables_for_test.go")
    defer f.Close()


    fmt.Println("writting head")
    f.WriteString("package sorting\n")
    f.WriteString("\n")

    f.WriteString("func getEmptyArray() []int64 {\n")
    f.WriteString("\treturn []int64{}\n")
    f.WriteString("}\n\n")

    fmt.Println("writting ordered")
    f.WriteString("func getOrderedArray() []int64 {\n")
    f.WriteString("\treturn []int64{")
    for i = 0; i < int64(max-1) ; i++ {
        f.WriteString(fmt.Sprintf("%s, ", strconv.FormatInt(int64(i), 10)))
    }
    f.WriteString(fmt.Sprintf("%s}\n", strconv.FormatInt(max-1, 10)))
    f.WriteString("}\n\n")

    fmt.Println("writting reverseOrdered")
    f.WriteString("func getReverseOrderedArray() []int64 {\n")
    f.WriteString("\treturn []int64{")
    for i = int64(max-1); i > 0; i-- {
        f.WriteString(fmt.Sprintf("%s, ", strconv.FormatInt(int64(i), 10)))
    }
    f.WriteString(fmt.Sprintf("%s}\n", strconv.FormatInt(0, 10)))
    f.WriteString("}\n\n")

    randHandler := rand.New(rand.NewSource(time.Now().Unix()))
    perm := randHandler.Perm(int(max))
    fmt.Println("writting shuffled")
    f.WriteString("func getShuffledArray() []int64 {\n")
    f.WriteString("\treturn []int64{")
    for i = 0; i < int64(max-1) ; i++ {
        f.WriteString(fmt.Sprintf("%s, ", strconv.FormatInt(int64(perm[i]), 10)))
    }
    f.WriteString(fmt.Sprintf("%s}\n", strconv.FormatInt(int64(perm[max-1]), 10)))

    f.WriteString("}\n\n")
    fmt.Println("done")
}

